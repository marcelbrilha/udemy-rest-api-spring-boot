package com.marcelbrilha.cursomc.config;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Header;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	private final ResponseMessage responseMessageStatusCode201 = customMessageHeaderLocation();
	private final ResponseMessage responseMessageStatusCode204Update = simpleMessage(204, "Atualização ok");
	private final ResponseMessage responseMessageStatusCode204Delete = simpleMessage(204, "Deleção ok");
	private final ResponseMessage responseMessageStatusCode403 = simpleMessage(403, "Não autorizado");
	private final ResponseMessage responseMessageStatusCode404 = simpleMessage(404, "Não encontrado");
	private final ResponseMessage responseMessageStatusCode422 = simpleMessage(422, "Erro de validação");
	private final ResponseMessage responseMessageStatusCode500 = simpleMessage(500, "Erro inesperado");

	private final List<ResponseMessage> globalResponseMessageGet = Arrays.asList(
		responseMessageStatusCode403,
		responseMessageStatusCode404,
		responseMessageStatusCode500
	);
	private final List<ResponseMessage> globalResponseMessagePost = Arrays.asList(
		responseMessageStatusCode201,
		responseMessageStatusCode403,
		responseMessageStatusCode422,
		responseMessageStatusCode500
	);
	private final List<ResponseMessage> globalResponseMessagePut = Arrays.asList(
		responseMessageStatusCode204Update,
		responseMessageStatusCode403,
		responseMessageStatusCode404,
		responseMessageStatusCode422,
		responseMessageStatusCode500
	);
	private final List<ResponseMessage> globalResponseMessageDelete = Arrays.asList(
		responseMessageStatusCode204Delete,
		responseMessageStatusCode403,
		responseMessageStatusCode404,
		responseMessageStatusCode500
	);

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
			.useDefaultResponseMessages(false)
			.globalResponseMessage(RequestMethod.GET, globalResponseMessageGet)
			.globalResponseMessage(RequestMethod.POST, globalResponseMessagePost)
			.globalResponseMessage(RequestMethod.PUT, globalResponseMessagePut)
			.globalResponseMessage(RequestMethod.DELETE, globalResponseMessageDelete)				
			.select()
			.apis(RequestHandlerSelectors.basePackage("com.marcelbrilha.cursomc.resources"))
			.paths(PathSelectors.any())
			.build()
			.apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {
		return new ApiInfo(
			"API do curso Spring Boot - Udemy",
			"Esta API é utilizada no curso de Spring Boot da Udemy", "Versão 1.0",
			"https://www.udemy.com/terms",
			new Contact("Marcel Brilha", "https://bitbucket.org/marcelbrilha", "marcelbrilha@gmail.com"),
			"Permitido uso para estudantes",
			"https://www.udemy.com/terms", Collections.emptyList()
		);
	}
	
	private ResponseMessage simpleMessage(int code, String msg) {
		return new ResponseMessageBuilder()
			.code(code)
			.message(msg)
			.build();
	}
	
	private ResponseMessage customMessageHeaderLocation() {
		Map<String, Header> map = new HashMap<>();
		map.put("location", new Header("location", "URI do novo recurso", new ModelRef("string")));
		
		return new ResponseMessageBuilder()
			.code(201)
			.message("Recurso criado")
			.headersWithDescription(map)
			.build();
	}
}
